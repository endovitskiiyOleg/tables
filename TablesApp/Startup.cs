﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TablesApp.Startup))]
namespace TablesApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
