﻿var mymodal = angular.module('mymodal', ['ui.bootstrap']);

mymodal.controller('MainCtrl', function ($scope, $uibModal, $log) {
    $scope.items = ['item1', 'item2', 'item3'];
    $scope.animationsEnabled = true;
    $scope.open = function (size) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'addNewFieldModal.cshtml',
            controller: function ($scope, $uibModalInstance, items) {
                $scope.items = items;
                $scope.selected = {
                    item: $scope.items[0]
                };

                $scope.ok = function () {
                    modalInstance.close($scope.selected.item);
                }

                $scope.cancel = function () {
                    modalInstance.dismiss('cancel');
                };
            },
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };
});
